using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

public class PlaneMovements : MonoBehaviour
{
    public float speed = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // get input from the user
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // rotate the plane
        Vector3 rotation = new Vector3(verticalInput, 0, -horizontalInput) * speed * Time.deltaTime;

        transform.Rotate(rotation);
    }
}
