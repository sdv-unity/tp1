using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public GameObject respawnPoint;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = respawnPoint.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // check if the ball is out of bounds
        if (transform.position.y < -10)
        {
            Respawn();
        }
    }

    public void Respawn()
    {
        transform.position = respawnPoint.transform.position;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
