using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatus : MonoBehaviour
{
    public float timeCount = 0;
    public GameObject timeCountLabel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private String FormatTime(float time)
    {
        int miliseconds = (int)(time * 1000) % 1000;
        int seconds = (int)time % 60;
        int minutes = (int)time / 60;
        return String.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, miliseconds);
    }

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;
        // Text mesh pro
        timeCountLabel.GetComponent<TMPro.TextMeshProUGUI>().text = "Time: " + FormatTime(timeCount);
    }

    public void Win()
    {
        timeCount = 0;
    }
}
